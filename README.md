Laevateinn
=========

Laevateinn is the software controlling [VRC](http://www.vexrobotics.com/wiki/Skyrise) Team [2993](http://megabots.intechchs.org)A's robot, Shazbot! Expect everything to be broken constantly, especially just before a competition.

### Using ###

The repository is mostly a eclipse workspace. Just point the [PROSe](http://sourceforge.net/projects/purdueros/) IDE to the download path and import Laevateinn. Building should work out of the box. Be aware that any modifications have to be redistributed as per the [GNU GPL](https://bitbucket.org/Abex/laevateinn/src/master/Laevateinn/src/LICENSE?at=master).
