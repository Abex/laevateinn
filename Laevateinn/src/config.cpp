/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#include "config.hpp"
#include "util.hpp"

// stuff for global.hpp
bool Running::competition;
Running::connection::e Running::connection::value;
Running::joyCount::e Running::joyCount::value;
Running::watchDogState::e Running::watchDogState::value;
AutonState::e AutonState::value = AutonState::Simple;

void debugMenu();
void getConfigPot()
{
  /*
   * OFF
   * MENU
   * DETECT
   * COMPETITION - FORCE VEXNET & 2 JOY
   */
  int place = analogRead(confPot)/(4096/4);
  switch(place)
  {
  case 0:
    Running::competition = true;
    Running::connection::value = Running::connection::VexNET;
    Running::joyCount::value = Running::joyCount::Dual;
    break;
  case 1:
    break;
  case 2:
    debugMenu();
    break;
  default://off
    Running::watchDogState::value = Running::watchDogState::Off;
    break;
  }
}
