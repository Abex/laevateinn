/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MENU_H_
#define MENU_H_

void debugMenu();

struct Menu;

namespace Menus
{
  void init();
}

struct MenuItem
{
  char* string;
  bool isMenu;
  Menu* menu;
};
struct FunctionItem
{
  char* string;
  bool isMenu;
  void (*func)();
};
typedef void(*FunctionMenu)();
struct InputMenu
{
  Menu* up;
  bool isFloat;
  void* num;
};
struct Menu
{
  void **items;
  int length;
  // Items is
  //length>0: Pointer to Array of MenuItem or FunctionItem
  //length==0 Pointer to pointer to function returning void
  //length=-1 Pointer to InputMenu
};

void runMenu();

extern Menu** menus;

#endif //MENU_H_
