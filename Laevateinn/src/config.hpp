/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#ifndef CONFIG_H_
#define CONFIG_H_

/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

/* =============================================================================================
  DO NOT EDIT THIS FILE  DO NOT EDIT THIS FILE   DO NOT EDIT THIS FILE  DO NOT EDIT THIS FILE

This (config.h) is automatically build by M4 from config.h.m4 . Edit config.h.m4 to make config
changes.

============================================================================================= */ 










//# PORT(name,mode,num)
// mode = INPUT, INPUT_ANALOG, INPUT_FLOATING, OUTPUT, or OUTPUT_OD

#include "util.hpp"

#ifdef NOTDEFINED
  enum portType {
    ANL_IN,        // input_analogue
    ANL_IN_FLOAT,  // input_floating
    DIG_IN,        // input
    DIG_OUT,       // output
    DIG_OUT_DRAIN, // output_od
    MOTOR          // na
  }
  typedef MultiJoystickAnalog MJoyAnl;
  typedef MultiJoystickDigital MJoyDig;
  /**
   * Defines a port for use and initilizes it.
   *
   * @param mode ANL_IN ANL_IN_FLOAT DIG_IN DIG_OUT DIG_OUT_DRAIN
   * @param number Pin # as on the cortex
  */
  void PORT(void* name,portType mode,unsigned char number);
#endif



/* 
#define NAME 
#define   //  
 PM_()
 // TYPE 
 PM_TYPE(NAME)
 */

#define MAJOR 2
#define MINOR 2

#define LCD uart2
#define serial uart1

#define IME_COUNT 6

#define lifterLIME 0
#define lifterRIME 1
#define driveFRIME 2
#define driveRRIME 3
#define driveRLIME 4
#define driveFLIME 5



#define confPot 1 // ANL_IN 
 
// analog first

#define powerExp 2 // ANL_IN 
 


#define lifterRPot 3 // ANL_IN 
 


#define lifterLPot 4 // ANL_IN 
 



#define driveFR 4 // MOTOR 
 
 // DRIVE

#define driveFL 5 // MOTOR 
 


#define driveRR 6 // MOTOR 
 


#define driveRL 7 // MOTOR 
 



#define lifterRA 2 // MOTOR 
 
//lifter

#define lifterRB 3 // MOTOR 
 


#define lifterLA 8 // MOTOR 
 
//different power bank

#define lifterLB 9 // MOTOR 
 



#define lifterRStop 4 // DIG_IN 
 


#define lifterLStop 5 // DIG_IN 
 



#define gripperAct 6 // DIG_OUT 
 


#define gripperSw 11 // DIG_IN 
 


//
#define lineFollowerLeft 6 // ANL_IN 
 

//
#define lineFollowerCenter 7 // ANL_IN 
 


#define lineFollowerRight 8 // ANL_IN 
 



#define accelX 7 // ANL_IN 
 


#define clawHasObjSw 11 // DIG_IN 
 


#define pressureAnl 6 // ANL_IN 
 


//sonars & followers


#define statusLEDR 2 // DIG_OUT 
 
 // Misc

#define statusLEDL 3 // DIG_OUT 
 


MJoyAnl driveX = {{1,2},{1,2},{1,2}};
MJoyAnl driveY = {{1,1},{1,1},{1,1}};
MJoyAnl driveR = {{1,4},{1,4},{1,3}};

MJoyAnl lifterU = {{1,3},{2,2},{1,3}};
MJoyAnl driveX2 = {{0,0},{2,3},{0,0}};
MJoyAnl driveY2 = {{0,0},{2,4},{0,0}};
MJoyAnl driveR2 = {{0,0},{0,0},{0,0}};
MJoyDig driveRR2= {{0,0,0},{2,5,JOY_UP},{0,0,0}};
MJoyDig driveRL2= {{0,0,0},{2,5,JOY_DOWN},{0,0,0}};

MJoyDig gripperOn = {{1,6,JOY_UP},{2,6,JOY_UP},{1,6,JOY_UP}};
MJoyDig gripperOff= {{1,6,JOY_DOWN},{2,6,JOY_DOWN},{1,6,JOY_DOWN}};
MJoyDig playMusicButton = {{1,7,JOY_LEFT},{1,7,JOY_LEFT},{1,7,JOY_LEFT}};
MJoyDig verticalAssistOff = {{1,8,JOY_RIGHT},{2,8,JOY_RIGHT},{1,8,JOY_RIGHT}};

MJoyDig armOverrideToggle = {{1,8,JOY_DOWN},{2,8,JOY_DOWN},{1,8,JOY_DOWN}};

#define setupPorts() {pinMode(confPot+12,INPUT_ANALOG);pinMode(powerExp+12,INPUT_ANALOG);pinMode(lifterRPot+12,INPUT_ANALOG);pinMode(lifterLPot+12,INPUT_ANALOG);pinMode(lifterRStop,INPUT);pinMode(lifterLStop,INPUT);pinMode(gripperAct,OUTPUT);pinMode(gripperSw,INPUT);pinMode(lineFollowerLeft+12,INPUT_ANALOG);pinMode(lineFollowerCenter+12,INPUT_ANALOG);pinMode(lineFollowerRight+12,INPUT_ANALOG);pinMode(accelX+12,INPUT_ANALOG);pinMode(clawHasObjSw,INPUT);pinMode(pressureAnl+12,INPUT_ANALOG);pinMode(statusLEDR,OUTPUT);pinMode(statusLEDL,OUTPUT);} // DO NOT EDIT THIS LINE
#endif /* CONFIG_H_ */
