/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#ifndef CONFIG_H_
#define CONFIG_H_

include(configMacroDefs.m4)

/* PORT(NAME,TYPE,PORT) */

#define MAJOR 2
#define MINOR 2

#define LCD uart2
#define serial uart1

#define IME_COUNT 6

#define lifterLIME 0
#define lifterRIME 1
#define driveFRIME 2
#define driveRRIME 3
#define driveRLIME 4
#define driveFLIME 5


PORT(confPot,ANL_IN,1)// analog first
PORT(powerExp,ANL_IN,2)
PORT(lifterRPot,ANL_IN,3)
PORT(lifterLPot,ANL_IN,4)

PORT(driveFR,MOTOR,4) // DRIVE
PORT(driveFL,MOTOR,5)
PORT(driveRR,MOTOR,6)
PORT(driveRL,MOTOR,7)

PORT(lifterRA,MOTOR,2)//lifter
PORT(lifterRB,MOTOR,3)
PORT(lifterLA,MOTOR,8)//different power bank
PORT(lifterLB,MOTOR,9)

PORT(lifterRStop,DIG_IN,4)
PORT(lifterLStop,DIG_IN,5)

PORT(gripperAct,DIG_OUT,6)
PORT(gripperSw,DIG_IN,11)

//PORT(lineFollowerLeft,ANL_IN,6)
//PORT(lineFollowerCenter,ANL_IN,7)
PORT(lineFollowerRight,ANL_IN,8)

PORT(accelX,ANL_IN,7)
PORT(clawHasObjSw,DIG_IN,11)
PORT(pressureAnl,ANL_IN,6)

//sonars & followers

PORT(statusLEDR,DIG_OUT,2) // Misc
PORT(statusLEDL,DIG_OUT,3)

MJoyAnl driveX = {{1,2},{1,2},{1,2}};
MJoyAnl driveY = {{1,1},{1,1},{1,1}};
MJoyAnl driveR = {{1,4},{1,4},{1,3}};

MJoyAnl lifterU = {{1,3},{2,2},{1,3}};
MJoyAnl driveX2 = {{0,0},{2,3},{0,0}};
MJoyAnl driveY2 = {{0,0},{2,4},{0,0}};
MJoyAnl driveR2 = {{0,0},{0,0},{0,0}};
MJoyDig driveRR2= {{0,0,0},{2,5,JOY_UP},{0,0,0}};
MJoyDig driveRL2= {{0,0,0},{2,5,JOY_DOWN},{0,0,0}};

MJoyDig gripperOn = {{1,6,JOY_UP},{2,6,JOY_UP},{1,6,JOY_UP}};
MJoyDig gripperOff= {{1,6,JOY_DOWN},{2,6,JOY_DOWN},{1,6,JOY_DOWN}};
MJoyDig playMusicButton = {{1,7,JOY_LEFT},{1,7,JOY_LEFT},{1,7,JOY_LEFT}};
MJoyDig verticalAssistOff = {{1,8,JOY_RIGHT},{2,8,JOY_RIGHT},{1,8,JOY_RIGHT}};

MJoyDig armOverrideToggle = {{1,8,JOY_DOWN},{2,8,JOY_DOWN},{1,8,JOY_DOWN}};

`#'define setupPorts() {SETUPPORTSM} // DO NOT EDIT THIS LINE
`#'endif /* CONFIG_H_ */
