/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Sorry about the orginization of this file and lack of reusable code. One thing I have learned
 * is software development is making reusable code is hard. Makeing reusable code that is
 * efficient in most situation is just short of impossible. As this is a embedded STM32
 * most code is wrote with a speed > readability thought process.
 */

//#define PID

#include "global.hpp"
#include "config.hpp"
#include "update.hpp"
#include "util.hpp"
#include "math.h"
#include "imeReader.hpp"

int liftAssistMult=50;

using namespace Motor;
inline int motorToIME(int motorVal)
{
  return motorVal*28;
}
inline int IMEToMotor(int imeVal)
{
  return imeVal/20;
}
int sign(int val)
{
  if(val>0)return 1;
  if(val<0)return -1;
  return 0;
}

namespace Motor{
  // drive input vars
  int rx;
  int lx;
  int ry;

  int driveKP =650;// int division is so much faster
  int driveKPd=1000;
  int driveKI =25;
  int driveKId=1000;
  int driveIntegralMaxError=5000;
  int driveIntegralMinError=600;
  int driveKD =45;
  int driveKDd=1000;

  // arm joystick value (input var)
  int arm;
  // and motor output value

  bool jr=false;// was the arm read / should the autocorrect control the arm val now
  int rTarget; // pot value for where we should be
  int lTarget;
  int lAdj=0; // pot value for very bottom
  int rAdj=0;

  int rPotC; // pot current value
  int lPotC;

  bool rStopC; // bottom limit value
  bool lStopC;

  //do verticalAssist
  bool verticalAssist=true;
  bool override=false;

  const int delta=2400; // top to bottom pot difference
  const int deltaMax=50; // accuracy of lift
  const int deltaMin=50;

  int accelBase;
  bool doAccel;

  DriveMotor driveFRD,driveFLD,driveRRD,driveRLD;
}

int getXfromPot(int pot) // vertical assist - get ~ how forward we should be based on pot value
{//19.35 c/deg
  //1108.6733 = 1/((1/19.35)*(1)*(3.14159265/180))
  return 30*(1-cos(((double)pot-1200.0)/1108.6733));
}

void Motor::update()
{  //ARM
  rPotC=4095-analogRead(lifterRPot); //read vals
  lPotC=analogRead(lifterLPot);
  rStopC=dr(lifterRStop);
  lStopC=dr(lifterLStop);
  if(arm>-5 && arm<5) arm=0; // clean analog stick values

  if(arm==0) // no input on arm
  {//target
    if(jr==true)
    {//set target
      jr=false;
      rTarget=rPotC;
      lTarget=lPotC;
    }
    else
    {//adjust to target
      int rOffset = rTarget-rPotC; // calc offset -> speed
      if(rOffset>deltaMax) arm=rOffset/2;
      if(rOffset<deltaMin) arm=rOffset/4;
      int lOffset = lTarget-lPotC;
      if(lOffset>deltaMax) arm=lOffset/2;
      if(lOffset<deltaMin) arm=lOffset/4;
    }
  }
  else // we are reading from analog stick
    jr=true;
  if(rStopC&&lStopC)
  {//calibrate
    lAdj=lPotC;
    rAdj=rPotC;
  }

  if(arm>0 && lPotC>lAdj+delta) // top limiting
    arm=0;//top
  if(arm>0 && rPotC>rAdj+delta)
    arm=0;//top

  if(arm>-5 && arm<20) arm=0; // clean again
  if(jr && verticalAssist && false) // active movement
  {
    int x1,x2,y1,y2;//TODO fix
    y1 = lPotC-lAdj;
    y2 = y1+(imeVelocity[lifterRIME]/2);
    x1=getXfromPot(y1);
    x2=getXfromPot(y2);
    double slope;
    if(y2==y1)
      slope=0;
    else
      slope=((double)(x2-x1))/((double)(y2-y1));
    int delta = (int)(slope*liftAssistMult*arm);
    rx+=delta;
  }
  if(imeVelocity[lifterRIME]!=5) // arm bounce protection
  {
    int motorActual = imeVelocity[lifterRIME]/28;
    if(motorActual<-25 && arm>20) // going down telling up
    {
      arm=10;
      jr=true;
    }
    if(motorActual>35 && arm<-20) // going up telling down
    {
      arm=0;
      jr=true;
    }
  }
  if(dr(lifterRStop)&&arm<0&&!override)// set motor values (respecting lower limits)
    ms(lifterRA,lifterRB,0);
  else
    ms(lifterRA,lifterRB,arm);
  if(dr(lifterLStop)&&arm<0&&!override)
    ms(lifterLA,lifterLB,0);
  else
    ms(lifterLA,lifterLB,-arm);

  //DRIVE

  int accelDelta = analogReadCalibratedHR(accelX)-accelBase;
  if(abs(accelDelta)>500 && doAccel && arm==0)
  {
    rx+=-120;
  }

  driveFLD.target= -(rx+lx+ry); //calc & set drive
  driveFRD.target=  (rx-lx-ry);
  driveRLD.target=  (rx+lx-ry);
  driveRRD.target= -(rx-lx+ry);
#ifdef PID
  driveFLD.error=motorToIME(driveFLD.target)-imeVelocity[driveFLIME];
  driveFRD.error=motorToIME(driveFRD.target)-imeVelocity[driveFRIME];
  driveRLD.error=motorToIME(driveRLD.target)-imeVelocity[driveRLIME];
  driveRRD.error=motorToIME(driveRRD.target)-imeVelocity[driveRRIME];

  driveFLD.integral+=driveFLD.error;
  driveFRD.integral+=driveFRD.error;
  driveRLD.integral+=driveRLD.error;
  driveRRD.integral+=driveRRD.error;

  if(abs(driveFLD.error)<=driveIntegralMinError || abs(driveFLD.error)>driveIntegralMaxError) {driveFLD.integral=0;driveFLD.error=0;}
  if(abs(driveFRD.error)<=driveIntegralMinError || abs(driveFRD.error)>driveIntegralMaxError) {driveFRD.integral=0;driveFRD.error=0;}
  if(abs(driveRLD.error)<=driveIntegralMinError || abs(driveRLD.error)>driveIntegralMaxError) {driveRLD.integral=0;driveRLD.error=0;}
  if(abs(driveRRD.error)<=driveIntegralMinError || abs(driveRRD.error)>driveIntegralMaxError) {driveRRD.integral=0;driveRRD.error=0;}


  motorSet(driveFL,IMEToMotor(( (driveKP*driveFLD.error)/driveKPd) + ((driveKI*driveFLD.integral)/driveKId) +  ((driveKD*(driveFLD.error-driveFLD.previousError))/driveKDd)));
  motorSet(driveFR,IMEToMotor(( (driveKP*driveFRD.error)/driveKPd) + ((driveKI*driveFRD.integral)/driveKId) +  ((driveKD*(driveFRD.error-driveFRD.previousError))/driveKDd)));
  motorSet(driveRL,IMEToMotor(( (driveKP*driveRLD.error)/driveKPd) + ((driveKI*driveRLD.integral)/driveKId) +  ((driveKD*(driveRLD.error-driveRLD.previousError))/driveKDd)));
  motorSet(driveRR,IMEToMotor(( (driveKP*driveRRD.error)/driveKPd) + ((driveKI*driveRRD.integral)/driveKId) +  ((driveKD*(driveRRD.error-driveRRD.previousError))/driveKDd)));

  driveFLD.previousError=driveFLD.error;
  driveFRD.previousError=driveFRD.error;
  driveRLD.previousError=driveRLD.error;
  driveRRD.previousError=driveRRD.error;
#else
  ms(driveFL,driveFLD.target);
  ms(driveFR,driveFRD.target);
  ms(driveRL,driveRLD.target);
  ms(driveRR,driveRRD.target);
#endif
}
