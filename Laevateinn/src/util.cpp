/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util.hpp"
#include "global.hpp"

int joyAnl(MultiJoystickAnalog j)
{
  if(Running::connection::Tethered==Running::connection::value) return 0;
  if(Running::joyCount::Single==Running::joyCount::value) return joystickGetAnalog(j.Single.joystick,j.Single.axis);
  return joystickGetAnalog(j.Double.joystick,j.Double.axis);
}

bool joyDig(MultiJoystickDigital j)
{
  if(Running::connection::Tethered==Running::connection::value) return 0;
  if(Running::joyCount::Single==Running::joyCount::value) return joystickGetDigital(j.Single.joystick,j.Single.buttonGroup,j.Single.button);
  return joystickGetDigital(j.Double.joystick,j.Double.buttonGroup,j.Double.button);
}
void ms(unsigned char motor,int val)
{
  if(abs(val)<5)val=0;
  motorSet(motor,val);
}
void ms(unsigned char motor,unsigned char motor2,int val)
{
  if(abs(val)<5)val=0;
  motorSet(motor,val);
  motorSet(motor2,val);
}
void joyCleanRef(int* val)
{
  if(*val<10 && *val>-10) *val=0;
}
bool dr(unsigned char s)
{
  return !digitalRead(s);
}
void ds(unsigned char s,bool v)
{
  digitalWrite(s,!v);
}
