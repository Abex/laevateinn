/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#include "imeReader.hpp"
#include "update.hpp"

void autonUpdateTask(void* ignore)
{
  while(isAutonomous())
  { // allow the auton thread to be sync
    Motor::update();
    delay(20);
  }
  taskDelete(NULL);// lets not restart the cortex
  while(1)
  {
    delay(20);
  }
}

void waitForIME(unsigned char IME,int revolutions) //24.5 for HS-IME
{
  int targetRevs=imeCounts[IME];
  int cRevs=targetRevs;
  targetRevs+=revolutions;
  while((revolutions<0)?targetRevs<cRevs:targetRevs>cRevs)
  {
    cRevs = imeCounts[IME];
    delay(10);
  }
}
void testAuton()
{
  digitalWrite(gripperAct,1);
  Motor::rx=127;
  delay(400);
  Motor::rx=-20;
  delay(30);
  Motor::rx=0;
  digitalWrite(gripperAct,0);
  delay(80);
  Motor::jr=false;
  Motor::arm=127;
  Motor::ry=127;
  while(analogRead(lifterLPot)<3300)
    delay(20);
  Motor::arm=0;
  delay(30);
  Motor::ry=0;
}
void driveBackAuton()
{
  Motor::rx=-127;
  delay(200);
  Motor::rx=0;
}
void autonomous()
{
  postInit();
  taskCreate(autonUpdateTask,TASK_DEFAULT_STACK_SIZE,NULL,TASK_PRIORITY_DEFAULT);
  driveBackAuton();
}
