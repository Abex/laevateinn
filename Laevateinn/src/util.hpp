/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTIL_HPP_
#define UTIL_HPP_

namespace Running
{
  extern bool competition;
  namespace connection {enum e{VexNET,Tethered}; extern e value;}
  namespace joyCount {enum e{Dual,Single}; extern e value;}
  namespace watchDogState {enum e{Running, Disabled, Off, Error}; extern e value;}
}
namespace AutonState {
  enum e {
    Simple = 0,
    S2 = 1,
    COUNT = 2
  };
  extern e value;
}

struct JAnalog{
  unsigned char joystick;
  unsigned char axis;
};
struct MultiJoystickAnalog {
  JAnalog Single;
  JAnalog Double;
  JAnalog PC;
};
struct JDigital {
  unsigned char joystick;
  unsigned char buttonGroup;
  unsigned char button;
};
struct MultiJoystickDigital {
  JDigital Single;
  JDigital Double;
  JDigital PC;
};
struct DriveMotor {
  int target;
  int error;
  int integral;
  int previousError;
};

#define MJoyAnl const static MultiJoystickAnalog
#define MJoyDig const static MultiJoystickDigital
int joyAnl(MultiJoystickAnalog j);
bool joyDig(MultiJoystickDigital j);
void joyCleanRef(int* val);
bool dr(unsigned char s);
void ms(unsigned char motor,unsigned char motor2,int val);
void ms(unsigned char motor,int val);
void ds(unsigned char s,bool v);

#endif /* UTIL_HPP_ */
