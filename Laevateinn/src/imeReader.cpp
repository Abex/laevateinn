/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#include "imeReader.hpp"

int imeVelocity[IME_COUNT]; //access these
int imeCounts[IME_COUNT];

long imeLastUpdate;
//last time IMEs were updated (in micros())

int imeCount;// actual IMEs in the system
bool imesInited=false; // are they usable
int imeFails=0; // fuzzy count for restarting imes

// get velocity & set to IME_ERROR_VALUE if error
void imeGetVelocityWithD(int addr, int* ptr)
{
  if(addr>=imeCount) // error if the ime isn't in the system
  {
    imeFails+=20;
    *ptr= IME_ERROR_VALUE;
  }
  else if(!imeGetVelocity(addr,ptr))
  {
    *ptr = IME_ERROR_VALUE;
    imeFails+=80;
  }
  else if(*ptr==IME_ERROR_VALUE) // make sure the ime doesn't return IME_ERROR_VALUE
    (*ptr)++;
  --imeFails;
}
// get count & set to IME_ERROR_VALUE if error
void imeGetWithD(int addr, int* ptr)
{
  if(addr>=imeCount)
  {
    imeFails+=20;
    *ptr= IME_ERROR_VALUE;
  }
  else if(!imeGet(addr,ptr))
  {
    *ptr = IME_ERROR_VALUE;
    imeFails+=80;
  }
  else if(*ptr==IME_ERROR_VALUE)
    (*ptr)++;
  --imeFails;
}
void initImes()
{
  if(imesInited) //restart if running
  {
    imesInited=false;
    imeShutdown();
    taskDelay(300);// have to have a delay
  }
  imeCount = imeInitializeAll();
  if(imeCount != IME_COUNT)
  {
    lcdPrint(LCD,2,"IME COUNT ERR %d",imeCount);
    return;
  }
  imesInited=true;
}
void imeResetSafe(int ime)
{ // to make sure we arn't restarting
  if(imesInited) imeReset(ime);
}

void imeReaderTask(void* ignore)
{
  initImes();
  while(Running::watchDogState::value!=Running::watchDogState::Off)
  {
    imeLastUpdate=micros();
    for(int i=0;i<IME_COUNT;i++) //iterate through all of the imes
    {
      imeGetVelocityWithD(i,&imeVelocity[i]);
      imeGetWithD(i,&imeCounts[i]);
      delay(2);
    }
    if(imeFails>800 && false) // restart the IMEs if too many failed readings
      initImes();
    delay(20);
  }
  while(true)
    delay(20);
}
