/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Dear Future Self,
 * You are probably here because it is using too much memory.
 * Don't try fixing this, just rewrite it. With less macros hopefully?
 */

#include "menu.hpp"
#include "update.hpp"
#include "config.hpp"
#include "global.hpp"
#include "stdlib.h"
#include "math.h"
#include "imeReader.hpp"
#include "update.hpp"

#pragma GCC diagnostic ignored "-Wwrite-strings"

Menu* currentMenu = NULL;
int currentMenuPlace=0;

extern int liftAssistMult;

//macros to create the structs
#define newMenuFunction(func) (void(*)())([]()func)
#define newMenu(name,_len) name.length = _len; name.items = (void**) malloc(sizeof(void*)*_len)
#define newMenuItem(name,text,_menu) ([&]()->void*{MenuItem *MenuItem_ ## name = (MenuItem*)malloc(sizeof(MenuItem)); MenuItem_ ## name->isMenu=true; MenuItem_ ## name->string=text; MenuItem_ ## name->menu=_menu; return (void*)(MenuItem_ ## name );})()
#define newFMenuItem(name,text,function) ([]()->void*{FunctionItem *MenuItem_ ## name = (FunctionItem*)malloc(sizeof(FunctionItem)); MenuItem_ ## name->isMenu=false; MenuItem_ ## name->string=text; MenuItem_ ## name->func=function; return (void*)(MenuItem_ ## name);})()
#define newFMenu(name,function) name.length = 0; void *fp_ ## name = (void*)&function; name.items = (void**)malloc(sizeof(int)); *(name.items)=fp_ ## name ;
#define newIMenu(name,number,isFloat,up) name.length = -1; InputMenu* InputMenu_ ## name =  (InputMenu*)malloc(sizeof(InputMenu));* InputMenu_ ## name = { up , isFloat ,(void*) number }; name.items = (void**) (InputMenu_ ## name );
void loadMenu(Menu* menu);
void releaseButtons();
void displayBattery();

namespace Menus
{ // keep all of the shit in here to not polute the global namespace with crap
  Menu sensors;
  Menu debugMenu;
  Menu lineFollowers;
  Menu pots;
  Menu driveImes;
  Menu driveImeCounts;
  Menu voltage;
  Menu lifterImes;
  //Menu tuning;
  Menu tuningLiftAssist;
  //Menu drivePID;
  Menu drivePIDKI;
  Menu drivePIDKP;
  Menu drivePIDKD;
  Menu drivePIDMaxVal;
  Menu drivePIDMinVal;
  Menu drivePIDErrors;
  Menu miscSensors;

  void lineFollowersFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::sensors); // center may be depressed when entering fn from selecting it in the menu
    lcdSetText(LCD,1,"Left Center Rght");
    lcdPrint(LCD,2,"%04d  %04d  %04d",analogRead(lineFollowerLeft),analogRead(lineFollowerCenter),analogRead(lineFollowerRight));
  }

  void potsFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::sensors);
    lcdSetText(LCD,1,"Left Right  Conf");
    lcdPrint(LCD,2,"%04d  %04d  %04d",analogRead(lifterLPot),analogRead(lifterRPot),analogRead(confPot));
  }

  void voltageFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::debugMenu);
    displayBattery();
  }

  void driveImeFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::sensors);
    lcdPrint(LCD,1,"%+05d      %+05d",imeVelocity[driveFLIME],imeVelocity[driveFRIME]);
    lcdPrint(LCD,2,"%+05d      %+05d",imeVelocity[driveRLIME],imeVelocity[driveRRIME]);
  }

  void driveErrorFunction()
  {
    loadMenu(&Menus::debugMenu);/*
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::drivePID);
    lcdPrint(LCD,1,"%+05d      %+05d",Motor::driveFLD.error,Motor::driveFRD.error);
    lcdPrint(LCD,2,"%+05d      %+05d",Motor::driveRLD.error,Motor::driveRRD.error);*/
  }

  void driveImeCountFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && (b&LCD_BTN_RIGHT))
    {
      imeResetSafe(driveFRIME);
      imeResetSafe(driveFLIME);
      imeResetSafe(driveRRIME);
      imeResetSafe(driveRLIME);
    }
    if(b && (b&LCD_BTN_LEFT)) return loadMenu(&Menus::sensors);
    lcdPrint(LCD,1,"%+05d %+05d",imeCounts[driveFLIME],imeCounts[driveFRIME]);
    lcdPrint(LCD,2,"%+05d %+05d",imeCounts[driveRLIME],imeCounts[driveRRIME]);
  }

  void lifterImesFunction()
  {
    int b = lcdReadButtons(LCD);
    if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::sensors);
    lcdSetText(LCD,1,"");
    lcdPrint(LCD,2,"%+05d      %+05d",imeVelocity[lifterLIME],imeVelocity[lifterRIME]);
  }

  void miscSensorFunction()
  {
	  int b = lcdReadButtons(LCD);
	  if(b && !(b&LCD_BTN_CENTER)) return loadMenu(&Menus::sensors);
	  lcdSetText(LCD,1,"Pressure Accel");
	  lcdPrint(LCD,2,"%+05d      %+05d",analogReadCalibratedHR(accelX),Motor::accelBase);//analogReadCalibratedHR(accelX));
  }

  void init()
  { // create & allocate the crap
    newMenu(debugMenu,3);
    newMenu(sensors,7);
    /*newMenu(tuning,3);
    newMenu(drivePID,7);
    newIMenu(tuningLiftAssist,&liftAssistMult,false,&tuning);
    newIMenu(drivePIDKP ,&Motor::driveKP,false,&drivePID);
    newIMenu(drivePIDKI ,&Motor::driveKI,false,&drivePID);
    newIMenu(drivePIDKD,&Motor::driveKD,false,&drivePID);
    newIMenu(drivePIDMaxVal ,&Motor::driveIntegralMaxError,false,&drivePID);
    newIMenu(drivePIDMinVal ,&Motor::driveIntegralMinError,false,&drivePID);*/
    debugMenu.items[0] = newFMenuItem(dbmenuUp,"Up to main",newMenuFunction({currentMenu=NULL;}));
    debugMenu.items[1] = newMenuItem(dbmenuVoltage,"Voltages",&voltage);
    debugMenu.items[2] = newMenuItem(dbmenuSensors,"Sensors",&sensors);
    //debugMenu.items[3] = newMenuItem(dbmenuTuning,"Tuning",&tuning);
    sensors.items[0] = newMenuItem(sensorMenuUp,"Up to debug",&debugMenu);
    sensors.items[1] = newMenuItem(sensorMenuLf,"Line Followers",&lineFollowers);
    sensors.items[2] = newMenuItem(sensorMenuPots,"Pots",&pots);
    sensors.items[3] = newMenuItem(sensorDriveIme,"Drive IMEs",&driveImes);
    sensors.items[4] = newMenuItem(sensorDriveImeCount,"Drive IME Count",&driveImeCounts);
    sensors.items[5] = newMenuItem(sensorLifterIme,"Lifter IMEs",&lifterImes);
    sensors.items[6] = newMenuItem(sensorMiscSensors,"Misc.",&miscSensors);
    /*tuning.items[0] = newMenuItem(tuningMenuUp,"Up to debug",&debugMenu);
    tuning.items[1] = newMenuItem(tuningLiftAssistItem,"Lift Assist",&tuningLiftAssist);
    tuning.items[2] = newMenuItem(tuningDrivePIDItem,"Drive PID",&drivePID);
    drivePID.items[0] = newMenuItem(drivePIDUp,"Up to Tuning",&tuning);
    drivePID.items[1] = newMenuItem(drivePIDItemKP,"Kp",&drivePIDKP);
    drivePID.items[2] = newMenuItem(drivePIDItemKI,"Ki",&drivePIDKI);
    drivePID.items[3] = newMenuItem(drivePIDItemKD,"Kd",&drivePIDKD);
    drivePID.items[4] = newMenuItem(drivePIDItemMaxVal,"Error Max",&drivePIDMaxVal);
    drivePID.items[5] = newMenuItem(drivePIDItemMinVal,"Error Min",&drivePIDMinVal);
    drivePID.items[6] = newMenuItem(drivePIDError,"Error Vals",&drivePIDErrors);*/
    newFMenu(lineFollowers,lineFollowersFunction);
    newFMenu(pots,potsFunction);
    newFMenu(driveImes,driveImeFunction);
    newFMenu(voltage,voltageFunction);
    newFMenu(lifterImes,lifterImesFunction);
    newFMenu(driveImeCounts,driveImeCountFunction);
    newFMenu(drivePIDErrors,driveErrorFunction);
    newFMenu(miscSensors,miscSensorFunction);
  }
};

void renderCurrentMenu()
{
  if(currentMenu==NULL) return; // Null ptr checks
  if(currentMenu->length<=0) return;
  lcdSetText(LCD,2,"<---  VVVV  --->");
  if(currentMenu->items!=NULL && currentMenu->items[currentMenuPlace]!=NULL )
    lcdSetText(LCD,1,((MenuItem*)currentMenu->items[currentMenuPlace])->string);
}

void releaseButtons()
{ // wait for no buttons to be pressed
  while(lcdReadButtons(LCD))
    delay(50);
}

void debugMenu()
{ // enter debug menu
  releaseButtons();
  currentMenu=&Menus::debugMenu;
  renderCurrentMenu();
}

void loadMenu(Menu* menu)
{ // open menu
  releaseButtons();
  currentMenu=menu;
  renderCurrentMenu();
}

void runMenu() // actually do shit
{
  if(currentMenu==NULL) return;
  if(currentMenu->length==-1) // Number input
  {//TODO: Float support
    if(currentMenu->items!=NULL && ((InputMenu*)(currentMenu->items))->up!=NULL)
    {
      int b = lcdReadButtons(LCD); // write str
      char str[16];
      int c = snprintf(str,16," E- %11d",*((int*)(((InputMenu*)(currentMenu->items))->num)));
      if(millis()%500<200)
        str[currentMenuPlace]='#';//blinking pointer (not ptr)
      lcdSetText(LCD,1,str);
      if(b&LCD_BTN_RIGHT) // move right
      {
        currentMenuPlace++;
        if(currentMenuPlace>=c)
          currentMenuPlace=0;
      } else
      if(b&LCD_BTN_LEFT)
      {
        currentMenuPlace--;
        if(currentMenuPlace<0)
          currentMenuPlace=c;
      } else
      if(b&LCD_BTN_CENTER) // increment
      {
        if(currentMenuPlace==1) // actually exti
        {
          return loadMenu(((InputMenu*)currentMenu->items)->up);
        } else
        if(currentMenuPlace==2)
        {
          (*(int*)(((InputMenu*)currentMenu->items)->num))*=-1;
        } else
        if(currentMenuPlace>2)//not blank tile
        {
          int p = pow(10,c-currentMenuPlace-1);// separate all but selected number
          if((*(int*)(((InputMenu*)currentMenu->items)->num))<0) p=-p;
          if((*(int*)(((InputMenu*)currentMenu->items)->num)/p) - (10*(*(int*)(((InputMenu*)currentMenu->items)->num)/(p*10)))==9)
            (*(int*)((InputMenu*)currentMenu->items)->num)-=(p)*10; // rollover
          (*(int*)((InputMenu*)currentMenu->items)->num)+=(p);//increment
        }
      }
      if(b)//dont double roll
      {
        releaseButtons();
      }
    }
    return;
  }
  if(!currentMenu->length) // function menu
  {
    if(((void(*)())(*(currentMenu->items)))!=NULL)
      ((void(*)())(*(currentMenu->items)))(); // null ptr && call
    return;
  }
  int lcdButtons = lcdReadButtons(LCD);//menu
  if(lcdButtons&LCD_BTN_LEFT) //move
  {
    releaseButtons();
    if(currentMenuPlace<=0)
      currentMenuPlace = currentMenu->length-1;
    else
      currentMenuPlace--;
    renderCurrentMenu();
  } else
  if(lcdButtons&LCD_BTN_RIGHT)
  {
    releaseButtons();
    if(currentMenuPlace>=currentMenu->length-1)
    {
      currentMenuPlace=0;
    }
    else
      currentMenuPlace++;
    renderCurrentMenu();
  } else
  if(lcdButtons&LCD_BTN_CENTER)
  { // load new menu (or fn)
    releaseButtons();
    if(currentMenuPlace>=currentMenu->length||currentMenuPlace<0)
    {
      currentMenuPlace=0;//array bound
      return;
    }
    if(((currentMenu->items[currentMenuPlace])!=NULL)
      &&(((MenuItem*)(currentMenu->items[currentMenuPlace]))->isMenu)) // load menu
    {
      currentMenu = ((MenuItem*)currentMenu->items[currentMenuPlace])->menu;
      currentMenuPlace=0;
      renderCurrentMenu();
    }
    else if(((MenuItem*)currentMenu->items[currentMenuPlace])!=NULL && // call function
        ((MenuItem*)currentMenu->items!=NULL) &&
        (((FunctionItem*)currentMenu->items[currentMenuPlace])->func!=NULL))
      ((FunctionItem*)currentMenu->items[currentMenuPlace])->func();
  }
}
