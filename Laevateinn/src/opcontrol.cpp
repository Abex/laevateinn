/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#include "config.hpp"
#include "update.hpp"
#include "stdlib.h"

extern bool musicRunning;
bool overrideLT=false;
bool gripperReleaseLT = false;

//void musicTask(void* ignore);

void operatorControl() {
  postInit(); // vexnet has started

  musicRunning=false;//isOnline(); // dont allow music to run if we are at competition

  Motor::doAccel=!isOnline();

	while (Running::watchDogState::value != Running::watchDogState::Off) {
	  Motor::rx = joyAnl(driveX); // get vals
	  Motor::ry = joyAnl(driveY);
	  Motor::lx = joyAnl(driveR);

	  if(Running::joyCount::value==Running::joyCount::Dual)
	  { // allow for 2nd driver to have fine control
	    Motor::rx+=joyAnl(driveX2)/2;
	    Motor::ry+=joyAnl(driveY2)/2;
	    Motor::lx+=(joyDig(driveRL2)-joyDig(driveRR2))*60; //buttons for rotate
	    /*int t=joyAnl(driveR2);
	    if(abs(t)>50)
	    {
	      t-=(t>0)?50:-50;
	      Motor::lx+=t;
	    }*/
	  }

	  if(joyDig(armOverrideToggle))
	  {
	    if(!overrideLT)
	    {
	      Motor::override=!Motor::override;
	    }
	    overrideLT=true;
	  }
	  else
	    overrideLT=false;

    joyCleanRef(&Motor::rx);
    joyCleanRef(&Motor::ry);
    joyCleanRef(&Motor::lx);

    Motor::arm = joyAnl(lifterU);
    Motor::verticalAssist = joyDig(verticalAssistOff);
    if(Motor::arm<0) Motor::arm/=3; // slow on the way down

    if(joyDig(gripperOn )) digitalWrite(gripperAct,!HIGH); // gripper control
    if(joyDig(gripperOff))
    {
      gripperReleaseLT=true;
      if(!digitalRead(clawHasObjSw))
        digitalWrite(gripperAct,!LOW);
    }
    else if(gripperReleaseLT)
    {
      digitalWrite(gripperAct,!LOW);
      gripperReleaseLT=false;
    }

    //if(!musicRunning && joyDig(playMusicButton)) taskCreate(musicTask,TASK_DEFAULT_STACK_SIZE,NULL,TASK_PRIORITY_DEFAULT);

    Motor::update();// actually compute motor values and set them based on input

    delay(20);
	}
	lcdSetText(LCD,2,"OFF");
	delay(100);
}
