/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMEREADER_HPP_
#define IMEREADER_HPP_

#include "config.hpp"
#include "limits.h"

#define IME_ERROR_VALUE 5 // IMEs seem to have a deadzone between 1-~50
// this is to not upset things expecting a always accurite value

extern int imeVelocity[IME_COUNT];
extern int imeCounts[IME_COUNT];

extern long imeLastUpdate;

void imeResetSafe(int ime);

void imeReaderTask(void* ignore);

#endif /* IMEREADER_HPP_ */
