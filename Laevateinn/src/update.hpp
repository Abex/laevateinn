/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UPDATE_HPP_
#define UPDATE_HPP_

#include "util.hpp"

namespace Motor
{
  extern int rx;
  extern int lx;
  extern int ry;

  extern int t;

  extern int arm;
  extern bool jr;
  extern int rTarget;
  extern int lTarget;
  extern int lAdj;
  extern int rAdj;

  extern int driveKP;// int division is so much faster
  extern int driveKPd;
  extern int driveKI;
  extern int driveKId;
  extern int driveIntegralMaxError;
  extern int driveIntegralMinError;
  extern int driveKD;
  extern int driveKDd;

  extern int rPotC;
  extern int lPotC;
  extern bool rStopC;
  extern bool lStopC;

  extern const int delta;
  extern const int deltaMax;
  extern const int deltaMin;
  extern const int slowDelta;

  extern bool verticalAssist;
  extern bool override;

  extern int accelBase;
  extern bool doAccel;

  extern DriveMotor driveFRD,driveFLD,driveRRD,driveRLD;

  void update();
}

#endif /* UPDATE_HPP_ */
