/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "global.hpp"
#include "config.hpp"
#include "util.hpp"
#include "lcd.hpp"
#include "menu.hpp"
#include "string.h"

/*
 * LCD Status
 * R    L
 * OFF  OFF  OFF
 * EStopped      off  on
 * NO Joysticks  fast fast
 * IME Error     on   count
 * Disabled      slow off
 * Teleop        slow slow
 * Auton         slow on
 */
typedef unsigned int uint;

inline bool slow();
inline bool fast();
void displayBattery();
extern int imeCount;
uint imeCountStart=0;
extern Menu* currentMenu;

using namespace Running;

void lcdTask(void* ignore)
{
  Menus::init();
  while(watchDogState::value != watchDogState::Off)
  {
    if(connection::value == connection::Tethered)// Tethered / no joystick
    { // assue error
      ds(statusLEDR,fast());
      ds(statusLEDL,fast());
    } else
    if(watchDogState::value == watchDogState::Error)
    { // x blips 700ms per + 2s blank
      ds(statusLEDR,HIGH);
      uint imeCountTimeT = 2000 + imeCount*700;
      if(imeCountStart+imeCountTimeT>millis())
      {//reset
        imeCountStart=millis();
      }
      if(imeCountStart+imeCountTimeT-2000>millis())
      {//2 sec sleep
        ds(statusLEDL,LOW);
      }else
        ds(statusLEDL,(millis()-imeCountStart)%700>350);
    }else
    if(watchDogState::value == watchDogState::Disabled)
    {
      ds(statusLEDR,slow());
      ds(statusLEDL,LOW);
    } else
    if(watchDogState::value == watchDogState::Running &&(isOnline()?!isAutonomous():true))
    {//teleop
      ds(statusLEDR,slow());
      ds(statusLEDL,slow());
    } else
    if(watchDogState::value == watchDogState::Running &&(isOnline()?isAutonomous():true))
    {
      ds(statusLEDR,slow());
      ds(statusLEDL,HIGH);
    }
    if(!currentMenu) //no menu
     {
      int lcdButtons = lcdReadButtons(LCD);
      if(lcdButtons&LCD_BTN_LEFT)//reinit
        postInit();
      else if(lcdButtons&LCD_BTN_CENTER)
        displayBattery();
      else if(lcdButtons&LCD_BTN_RIGHT)
      {
        debugMenu();
      }
    }
    if(currentMenu!=NULL)
    {
      runMenu();
    }
    delay(50);

  }
  ds(statusLEDR,LOW);
  ds(statusLEDL,LOW);
  taskDelete(NULL);
  while(1)
  {
    delay(50);
  }
}

void displayBattery()
{ // 29 - 59  120 - 0
  int drive = analogRead(powerExp);
  if(drive<280)drive=0; //hide <1volt
  int pressure = (((1-(float)analogRead(pressureAnl)/4096)-.41)/.3)*134;
  lcdClear(LCD);
  lcdPrint(LCD,1,"M D B    P %3d",pressure);
  lcdPrint(LCD,2,"%04.2f  %04.2f  %04.2f",((float)powerLevelMain())/1000,((float)drive)/280,((float)powerLevelBackup())/1000);
}

inline bool slow()
{
  return millis()%500>250;
}

inline bool fast()
{
  return millis()%200>100;
}
inline void ds(unsigned char sensor,bool set)
{
  digitalWrite(sensor,!set);
}
