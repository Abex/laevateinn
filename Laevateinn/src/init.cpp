/**
Copyright 2014 Max Weber

This file is part of Laevateinn.

Laevateinn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Laevateinn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Laevateinn.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "global.hpp"
#include "config.hpp"
#include "lcd.hpp"
#include "imeReader.hpp"
#include "update.hpp"
#include "util.hpp"

char nameStr[18];

TaskHandle lcdTaskHandle;

void eStop(unsigned char pin)
{return; // can trigger randomly
  motorStopAll();
  taskDelete(lcdTaskHandle);
  motorStopAll();
  digitalWrite(statusLEDR,LOW);
  digitalWrite(statusLEDL,HIGH);
  while(1){}//killed
}

void initializeIO() {
  setupPorts();
  digitalWrite(statusLEDR,LOW);
  digitalWrite(statusLEDL,LOW);
  setTeamName("Shazbots");
}

void initialize() {
  Running::watchDogState::value = Running::watchDogState::Off;
  sprintf(nameStr,"L VATEINN v%02d.%02d",MAJOR,MINOR);
  nameStr[1]=0x92; // AE char
  puts(nameStr);
  puts("\r\n");
  printf("Built %s %s",__DATE__,__TIME__);
  lcdInit(LCD);
  lcdSetBacklight(LCD,true);
  lcdClear(LCD);
  lcdSetText(LCD,1,nameStr);
  Running::watchDogState::value = Running::watchDogState::Disabled;
  //ioSetInterrupt(1,INTERRUPT_EDGE_RISING,eStop);
  //start tasks
  taskCreate(imeReaderTask,TASK_DEFAULT_STACK_SIZE,NULL,TASK_PRIORITY_DEFAULT);
  lcdTaskHandle = taskCreate(lcdTask,TASK_DEFAULT_STACK_SIZE,NULL,TASK_PRIORITY_DEFAULT);// also the status LEDs
}

bool BattOk() // all batteries charged & in
{
  return powerLevelMain()>7500 && powerLevelBackup()>8000 && analogRead(powerExp)>2100;
}

void postInit() //Once VexNET has started
{
  Running::watchDogState::value = Running::watchDogState::Running;
  updateJoystick();
  Running::competition = false;
  getConfigPot();
  lcdSetText(LCD,1,nameStr);
  analogCalibrate(accelX);
  Motor::accelBase = analogReadCalibratedHR(accelX);
  if(Running::competition)
  {
    lcdPrint(LCD,2,"Comp %s %s",isJoystickConnected(2)?"Joy":"NJ",BattOk()?"Batt":"Nb");
  }
  else
  {
    lcdPrint(LCD,2,"Debu %s %s",isJoystickConnected(2)?"2joy":"1joy",BattOk()?"Yeah":"No");
    /*usartInit(serial,115200, SERIAL_DATABITS_9 | SERIAL_PARITY_ODD);
    fputs("LVTr",serial);
    delay(5);
    if(fcount(serial))
    {
      char buf[5];
      char sb[]="LVTa";
      fgets(buf,4,serial);
      if(buf==sb)
        lcdPrint(LCD,2,"Hello!");
    }*/
  }

}

void updateJoystick()
{
  Running::connection::value = isJoystickConnected(1)?Running::connection::VexNET:Running::connection::Tethered;
  Running::joyCount::value = isJoystickConnected(2)?Running::joyCount::Dual:Running::joyCount::Single;
}
